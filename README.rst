=================
Le Moulin Gallery
=================

Django gallery that supports Youtube and Vimeo videos, uploaded videos, images and binary files.

.. contents::

Getting started
===============

Requirements
------------

Le Moulin Utils requires:

- Python 3.4
- Django >= 1.7
- sorl-thumbnail >= 11.12.1b
- lemoulin_utils

Installation
------------

You can get Le Moulin Utils by using pip::

    $ pip install -e git+https://bitbucket.org/lemoulin/django-lemoulin-gallery.git#egg=lemoulin_gallery

To enable `lemoulin_gallery` in your project you need to add it to `INSTALLED_APPS` in your projects
`settings.py` file::

    INSTALLED_APPS = (
        ...
        'lemoulin_gallery',
        ...
    )

Usage
=====
