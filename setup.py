from setuptools import setup, find_packages

version = __import__('lemoulin_gallery').__version__

packages = find_packages()

setup(
    name='django-lemoulin-gallery',
    packages=packages,
    version=version,
    description='Django gallery that supports Youtube and Vimeo videos, uploaded videos, images and binary files.',
    author='Yanik Proulx',
    author_email='yanikproulx@lemoulin.co',
    url='https://bitbucket.org/lemoulin/django-lemoulin-gallery',
    download_url='https://bitbucket.org/lemoulin/django-lemoulin-gallery',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
    scripts=[],
    license='LICENSE.txt',
    long_description=open('README.rst').read(),
    install_requires=[
        "Django >= 1.7",
        "Pillow >= 2.5.3",
        "sorl-thumbnail >= 11.12.1b",
        "requests",
        "django-lemoulin-utils >= 0.3",
    ],
    include_package_data = True,
)
