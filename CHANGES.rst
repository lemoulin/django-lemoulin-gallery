===============
Le Moulin utils
===============

*******
CHANGES
*******

.. contents:: Versions

v0.1
====
Initial release

v0.1.1
====
Use python-requests for Youtubbe & Vimeo API

v0.3
====
New Packaging

v0.4
====
Updated ModelForm to support Django 1.8

v0.5
====
Instagram oembed support

v0.6
====
Base64 save support
