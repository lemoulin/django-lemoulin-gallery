from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

# Signals and receivers
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

MEDIA_TYPE = (
	("image", _("Image")),
	("video", _("Video")),
	("file", _("File")),
)

VIDEO_SOURCE = (
	("youtube", _("YouTube")),
	("vimeo", _("Vimeo")),
)

IMAGE_SOURCE = (
	("instagram", _("Instagram")),
	("file", _("File")),
)


def get_media_file_path(instance, filename):
	from lemoulin_utils.files import clean_filename
	return "media/{filename}".format(filename=clean_filename(filename))


class Media(models.Model):
	media_type = models.CharField(verbose_name=_("Media Type"), max_length=25, choices=MEDIA_TYPE, blank=True, null=True, )
	title = models.CharField(verbose_name=_("Title"), max_length=1024, null=True, blank=True, )
	description = models.TextField(verbose_name=_("Description"), null=True, blank=True, )

	credit = models.CharField(verbose_name=_('Credit'), max_length=200, null=True, blank=True, )

	image = models.ImageField(verbose_name=_("Image"), upload_to=get_media_file_path, null=True, blank=True, )

	image_url = models.URLField(verbose_name=_("Image URL"), help_text=_("Instagram"), null=True, blank=True, )
	image_source = models.CharField(verbose_name=_('Image Source'), max_length=25, choices=IMAGE_SOURCE, null=True, blank=True, )
	image_embed_html = models.TextField(verbose_name=_("Image Embed HTML"), null=True, blank=True, )
	image_thumbnail_url = models.URLField(verbose_name=_("Image Thumbnail URL"), null=True, blank=True, )
	image_author_url = models.URLField(verbose_name=_("Image Author URL"), null=True, blank=True, )
	image_filename = models.CharField(verbose_name=_("Image orginal filemane"), max_length=255, null=True, blank=True, )
	image_base64 = models.TextField(verbose_name=_("Image source base64"), null=True, blank=True, )

	video = models.FileField(verbose_name=_("Video"), upload_to=get_media_file_path, null=True, blank=True, )
	video_thumbnail = models.ImageField(verbose_name=_("Video Thumbnail"), upload_to=get_media_file_path, null=True, blank=True, )

	video_url = models.URLField(verbose_name=_("Video URL"), help_text=_("Youtube or Vimeo URL"), null=True, blank=True, )
	video_source = models.CharField(verbose_name=_('Video Source'), max_length=25, choices=VIDEO_SOURCE, null=True, blank=True, )
	video_width = models.PositiveIntegerField(verbose_name=_("Video Width"), null=True, blank=True, )
	video_height = models.PositiveIntegerField(verbose_name=_("Video Height"), null=True, blank=True, )
	video_thumbnail_url = models.URLField(verbose_name=_("Video Thumbnail URL"), null=True, blank=True, )
	video_id = models.CharField(verbose_name=_("Video ID"), max_length=25, null=True, blank=True, )
	video_duration = models.PositiveIntegerField(verbose_name=_("Video Duration"), null=True, blank=True, )

	file = models.FileField(verbose_name=_("File"), upload_to=get_media_file_path, null=True, blank=True, )
	file_thumbnail = models.ImageField(verbose_name=_("File Thumbnail"), upload_to=get_media_file_path, null=True, blank=True, )

	position = models.PositiveSmallIntegerField(verbose_name=_("Position"), null=True, blank=True, )

	date_taken = models.DateTimeField(verbose_name=_("Date taken"), null=True, blank=True, )

	created = models.DateTimeField(auto_now_add=True, )
	updated = models.DateTimeField(auto_now=True, )

	def _set_media_data(self, set_title=True):
		from . import get_image_details, get_video_details

		if self.image or self.image_base64:
			self.media_type = "image"
			self.image_source = "file"

			if set_title and (not self.title):
				self.title = self.image.name

		elif self.image_url:
			self.media_type = "image"

			details = get_image_details(image_url=self.image_url)

			if details:
				if set_title and (not self.title):
					self.title = details.get("title")

				self.image_source = details.get("source")
				self.image_embed_html = details.get("embed_html")
				self.image_thumbnail_url = details.get("thumbnail_url")
				self.image_author_url = details.get("author_url")

		elif self.video:
			self.media_type = "video"
			self.video_source = "file"

			if set_title and (not self.title):
				self.title = self.video.name

		elif self.video_url:
			self.media_type = "video"

			details = get_video_details(video_url=self.video_url)

			if details:
				if set_title and (not self.title):
					self.title = details.get("title")

				if not self.description:
					self.description = details.get("description")

				self.video_width = details.get("width")
				self.video_height = details.get("height")
				self.video_thumbnail_url = details.get("thumbnail")
				self.video_duration = details.get("duration")
				self.video_id = details.get("video_id")
				self.video_source = details.get("source")

		elif self.file:
			self.media_type = "file"

			if set_title and (not self.title):
				self.title = self.file.name

	def _save_base64_data(self):
		"""Saves BASE64 content to filefield"""
		from django.core.files.base import ContentFile
		from base64 import b64decode

		# If we find this, strip it and all before
		header = "base64,"

		if (self.image_base64 and (not self.image)):
			# Strip "base64 header" if there
			pos = self.image_base64.find(header)
			if pos > 0:
				start = (pos+len(header))
				encoded_data = self.image_base64[start:]
			else:
				encoded_data = self.image_base64

			file_data = b64decode(encoded_data)
			file_content = ContentFile(file_data)

			self.image_base64 = None
			self.image.save(self.image_filename, file_content)

	class Meta:
		ordering = ["title"]
		verbose_name = _("Media")
		verbose_name_plural = _("Media")

	def __unicode__(self):
		return "%s (%s)" % (self.title, self.media_type)

@receiver(pre_save, sender=Media)
def set_media_data(sender, instance, **kwargs):
	"""Sets Media Data"""

	instance._set_media_data()
