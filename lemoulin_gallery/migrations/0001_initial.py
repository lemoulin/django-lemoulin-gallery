# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import lemoulin_gallery.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('media_type', models.CharField(max_length=25, blank=True, choices=[('image', 'Image'), ('video', 'Video'), ('file', 'File')], null=True, verbose_name='Media Type')),
                ('title', models.CharField(max_length=1024, blank=True, verbose_name='Title', null=True)),
                ('description', models.TextField(blank=True, verbose_name='Description', null=True)),
                ('credit', models.CharField(max_length=200, blank=True, verbose_name='Credit', null=True)),
                ('image', models.ImageField(blank=True, verbose_name='Image', null=True, upload_to=lemoulin_gallery.models.get_media_file_path)),
                ('video', models.FileField(blank=True, verbose_name='Video', null=True, upload_to=lemoulin_gallery.models.get_media_file_path)),
                ('video_thumbnail', models.ImageField(blank=True, verbose_name='Video Thumbnail', null=True, upload_to=lemoulin_gallery.models.get_media_file_path)),
                ('video_url', models.URLField(blank=True, help_text='Youtube or Vimeo URL', null=True, verbose_name='Video URL')),
                ('video_source', models.CharField(max_length=25, blank=True, choices=[('youtube', 'YouTube'), ('vimeo', 'Vimeo')], null=True, verbose_name='Video Source')),
                ('video_width', models.PositiveIntegerField(blank=True, verbose_name='Video Width', null=True)),
                ('video_height', models.PositiveIntegerField(blank=True, verbose_name='Video Height', null=True)),
                ('video_thumbnail_url', models.URLField(blank=True, verbose_name='Video Thumbnail URL', null=True)),
                ('video_id', models.CharField(max_length=25, blank=True, verbose_name='Video ID', null=True)),
                ('video_duration', models.PositiveIntegerField(blank=True, verbose_name='Video Duration', null=True)),
                ('file', models.FileField(blank=True, verbose_name='File', null=True, upload_to=lemoulin_gallery.models.get_media_file_path)),
                ('file_thumbnail', models.ImageField(blank=True, verbose_name='File Thumbnail', null=True, upload_to=lemoulin_gallery.models.get_media_file_path)),
                ('position', models.PositiveSmallIntegerField(blank=True, verbose_name='Position', null=True)),
                ('date_taken', models.DateTimeField(blank=True, verbose_name='Date taken', null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Media',
                'verbose_name': 'Media',
                'ordering': ['title'],
            },
            bases=(models.Model,),
        ),
    ]
