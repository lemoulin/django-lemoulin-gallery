# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lemoulin_gallery', '0003_auto_20150811_1238'),
    ]

    operations = [
        migrations.AddField(
            model_name='media',
            name='image_filename',
            field=models.CharField(null=True, verbose_name='Image orginal filemane', max_length=255, blank=True),
        ),
    ]
