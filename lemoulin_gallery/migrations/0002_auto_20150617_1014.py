# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lemoulin_gallery', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='media',
            name='image_author_url',
            field=models.URLField(null=True, blank=True, verbose_name='Image Author URL'),
        ),
        migrations.AddField(
            model_name='media',
            name='image_embed_html',
            field=models.TextField(null=True, blank=True, verbose_name='Image Embed HTML'),
        ),
        migrations.AddField(
            model_name='media',
            name='image_source',
            field=models.CharField(null=True, blank=True, verbose_name='Image Source', max_length=25, choices=[('upload', 'Upload'), ('instagram', 'Instagram')]),
        ),
        migrations.AddField(
            model_name='media',
            name='image_thumbnail_url',
            field=models.URLField(null=True, blank=True, verbose_name='Image Thumbnail URL'),
        ),
        migrations.AddField(
            model_name='media',
            name='image_url',
            field=models.URLField(null=True, blank=True, verbose_name='Image URL', help_text='Instagram'),
        ),
    ]
