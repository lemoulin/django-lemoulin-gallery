# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lemoulin_gallery', '0002_auto_20150617_1014'),
    ]

    operations = [
        migrations.AddField(
            model_name='media',
            name='image_base64',
            field=models.TextField(verbose_name='Image source base64', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='media',
            name='image_source',
            field=models.CharField(max_length=25, verbose_name='Image Source', choices=[('instagram', 'Instagram'), ('file', 'File')], null=True, blank=True),
        ),
    ]
