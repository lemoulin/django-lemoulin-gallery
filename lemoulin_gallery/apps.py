from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

# Used in __init__.py
class LeMoulinGalleryConfig(AppConfig):
	name = 'lemoulin_gallery'
	verbose_name = _("Gallery")
