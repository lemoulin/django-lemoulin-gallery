from __future__ import unicode_literals

import json
from django.views.generic import ListView, FormView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse

from . import models, forms

class MediaList(ListView):
	model = models.Media
	context_object_name = "media_items"
	template_name = "gallery/list.html"

	#@method_decorator(staff_member_required)
	@method_decorator(never_cache)
	def dispatch(self, request, *args, **kwargs):
		return super(MediaList, self).dispatch(request, *args, **kwargs)

	def get_queryset(self):
		qs = super(MediaList, self).get_queryset()

		return qs.filter(media_type="image")

	def render_to_response(self, context, **response_kwargs):
		response_data = []

		for media_item in context["object_list"]:
			response_data.append({"thumb": media_item.image.url, "image": media_item.image.url})

		return HttpResponse(json.dumps(response_data), content_type="application/json")

class MediaUpload(FormView):
	form_class = forms.MediaUploadForm
	template_name = "gallery/upload.html"

	#@method_decorator(staff_member_required)
	@method_decorator(never_cache)
	@method_decorator(csrf_exempt)
	def dispatch(self, request, *args, **kwargs):
		return super(MediaUpload, self).dispatch(request, *args, **kwargs)

	def get_initial(self):
		initial = super(MediaUpload, self).get_initial()

		initial["media_type"] = "image"

		return initial

	def form_invalid(self, form):
		import json
		from django.http import HttpResponse

		response_data = {}
		response_data["success"] = False
		response_data["errors"] = form.errors

		return HttpResponse(json.dumps(response_data), content_type="application/json")

	def form_valid(self, form):
		import json
		from django.http import HttpResponse

		media = form.save()

		response_data = {}
		response_data["success"] = True
		response_data["filelink"] = media.image.url

		return HttpResponse(json.dumps(response_data), content_type="application/json")
