# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

from . import views

urlpatterns = patterns(
	'',
	url(r'^ajax/media/upload/$', views.MediaUpload.as_view(), name="ajax_media_upload"),
	url(r'^ajax/media/list/$', views.MediaList.as_view(), name="ajax_media_list"),
)
