from __future__ import unicode_literals

from django.contrib import admin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.forms import ModelForm

if "suit_redactor" in settings.INSTALLED_APPS:
	from suit_redactor.widgets import RedactorWidget
elif "suit_ckeditor" in settings.INSTALLED_APPS:
	from suit_ckeditor.widgets import CKEditorWidget

from . import models

DEFAULT_GALLERY_MEDIA_TYPES = ["image", "image_url", "video_upload", "video_url", "file" ]

media_types = getattr(settings, "LE_MOULIN_GALLERY_MEDIA_TYPES", DEFAULT_GALLERY_MEDIA_TYPES)


class MediaForm(ModelForm):
	class Meta:
		if "suit_redactor" in settings.INSTALLED_APPS:
			widgets = {
				'description': RedactorWidget(editor_options={'lang': settings.LANGUAGE_CODE[:2]})
			}
		elif "suit_ckeditor" in settings.INSTALLED_APPS:
			widgets = {
				'description': CKEditorWidget(),
			}


class MediaAdmin(admin.ModelAdmin):
	form = MediaForm
	list_display = ("title", "media_type", "created", )
	ordering = ["title", "media_type", "created", ]
	search_fields = ["title", "media_type", "description", ]
	list_filter = ("media_type", "image_source", "video_source", )
	readonly_fields = (
		"image_source",
		"video_source",
		"video_width",
		"video_height",
		"video_thumbnail_url",
		"video_duration",
		"video_id",
		"media_type",
	)

	fieldsets = (
		(
			None,
			{
				'fields': ('title', 'description', 'credit', "position", )
			}
		),
	)

	if "image" in media_types:
		fieldsets += (
			(
				_("Image"),
				{
					'classes': ('collapse', ),
					'fields': ('image', )
				}
			),
		)

	if "image_url" in media_types:
		fieldsets += (
			(
				_("Image URL"),
				{
					'classes': ('collapse closed', ),
					'fields': ('image_url', "image_source", "image_embed_html", "image_thumbnail_url", "image_author_url", )
				}
			),
		)

	if "video_upload" in media_types:
		fieldsets += (
			(
				_("Uploaded Video"),
				{
					'classes': ('collapse', ),
					'fields': ('video', 'video_thumbnail', )
				}
			),
		)

	if "video_url" in media_types:
		fieldsets += (
			(
				_("Video URL"),
				{
					'classes': ('collapse closed', ),
					'fields': ('video_url', "video_source", "video_width", "video_height", "video_thumbnail_url", "video_duration", "video_id", )
				}
			),
		)

	if "file" in media_types:
		fieldsets += (
			(
				_("File"),
				{
					'classes': ('collapse', ),
					'fields': ('file', 'file_thumbnail', )
				}
			),
		)

	fieldsets += (
		(
			None,
			{
				'classes': ('collapse closed', ),
				'fields': ('media_type', )
			}
		),
	)

	class Media:
		if "GRAPPELLI_URL" in settings.INSTALLED_APPS:
			js = [
				settings.GRAPPELLI_URL+'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL+'tinymce_setup/tinymce_setup.js',
			]

admin.site.register(models.Media, MediaAdmin)


class MediaAdminInline(admin.StackedInline):
	form = MediaForm
	model = models.Media
	extra = 1
	readonly_fields = (
		"image_source",
		"video_source",
		"video_width",
		"video_height",
		"video_thumbnail_url",
		"video_duration",
		"video_id",
		"media_type",
	)

	fieldsets = (
		(
			None,
			{
				'fields': ('title', 'description', 'credit', "position", )
			}
		),
	)

	if "image" in media_types:
		fieldsets += (
			(
				_("Image"),
				{
					'classes': ('collapse', ),
					'fields': ('image', )
				}
			),
		)

	if "image_url" in media_types:
		fieldsets += (
			(
				_("Image URL"),
				{
					'classes': ('collapse closed', ),
					'fields': ('image_url', "image_source", "image_embed_html", "image_thumbnail_url", "image_author_url", )
				}
			),
		)

	if "video_upload" in media_types:
		fieldsets += (
			(
				_("Uploaded Video"),
				{
					'classes': ('collapse', ),
					'fields': ('video', 'video_thumbnail', )
				}
			),
		)

	if "video_url" in media_types:
		fieldsets += (
			(
				_("Video URL"),
				{
					'classes': ('collapse closed', ),
					'fields': ('video_url', "video_source", "video_width", "video_height", "video_thumbnail_url", "video_duration", "video_id", )
				}
			),
		)

	if "file" in media_types:
		fieldsets += (
			(
				_("File"),
				{
					'classes': ('collapse', ),
					'fields': ('file', 'file_thumbnail', )
				}
			),
		)

	fieldsets += (
		(
			None,
			{
				'classes': ('collapse closed', ),
				'fields': ('media_type', )
			}
		),
	)
	sortable_field_name = "position"

	class Media:
		if "GRAPPELLI_URL" in settings.INSTALLED_APPS:
			js = [
				settings.GRAPPELLI_URL+'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL+'tinymce_setup/tinymce_setup.js',
			]
