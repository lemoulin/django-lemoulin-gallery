# -*- coding: utf-8 -*-

from django import forms

from . import models

class MediaUploadForm(forms.ModelForm):
	"""
	Made to work with redactor
	It's form send a "file" field
	We match it to "image"
	"""
	image = forms.ImageField(required=False)
	file = forms.ImageField(required=False)

	def clean(self):
		cleaned_data = super(MediaUploadForm, self).clean()

		file = cleaned_data.get("file")
		cleaned_data["image"] = file

		return cleaned_data

	class Meta:
		model = models.Media
		fields = "__all__"
