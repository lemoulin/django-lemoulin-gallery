from __future__ import unicode_literals

from django import template

register = template.Library()


@register.inclusion_tag("gallery/_gallery.html", takes_context=True)
def show_gallery(context, media):

	request = context["request"]

	return {"context": context, "request": request, "media": media, }
